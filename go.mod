module git.sr.ht/~sircmpwn/builds.sr.ht

go 1.16

require (
	git.sr.ht/~sircmpwn/core-go v0.0.0-20230411141100-89b1b48997a8
	git.sr.ht/~sircmpwn/dowork v0.0.0-20221010085743-46c4299d76a1
	git.sr.ht/~sircmpwn/getopt v1.0.0 // indirect
	git.sr.ht/~sircmpwn/go-bare v0.0.0-20210406120253-ab86bc2846d9 // indirect
	github.com/99designs/gqlgen v0.17.29
	github.com/Masterminds/squirrel v1.5.4
	github.com/ProtonMail/go-crypto v0.0.0-20230411080316-8b3893ee7fca // indirect
	github.com/cloudflare/circl v1.3.2 // indirect
	github.com/emersion/go-message v0.16.0 // indirect
	github.com/emersion/go-sasl v0.0.0-20220912192320-0145f2c60ead // indirect
	github.com/emersion/go-smtp v0.16.0 // indirect
	github.com/fernet/fernet-go v0.0.0-20211208181803-9f70042a33ee // indirect
	github.com/go-redis/redis/v8 v8.11.5 // indirect
	github.com/gocelery/gocelery v0.0.0-20201111034804-825d89059344
	github.com/google/uuid v1.3.0
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/hashicorp/golang-lru/v2 v2.0.2 // indirect
	github.com/lib/pq v1.10.7
	github.com/prometheus/client_golang v1.15.0
	github.com/streadway/amqp v1.0.0 // indirect
	github.com/urfave/cli/v2 v2.25.1 // indirect
	github.com/vektah/dataloaden v0.3.0
	github.com/vektah/gqlparser/v2 v2.5.1
	golang.org/x/crypto v0.8.0 // indirect
	golang.org/x/tools v0.8.0 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
